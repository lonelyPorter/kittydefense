﻿using System;
using UnityEngine;

/// <summary>
/// This script - as of now - invokes a single event, telling the towers to shoot a bullet.
/// For different timings, different events could be created (WIP, trying to find a better solution)
/// </summary>
public class TowerTimer : MonoBehaviour
{
    //events that are sent to inform towers to shoot
    public static event Action HandleShootBullet = delegate { };

    private float timer = 0f;

    //these a default value
    public static float bulletDelay = 0.25f;

    private void OnDestroy()
    {
        HandleShootBullet = delegate { };
    }

    //FixedUpdate is called once every 0.02s ("fixed timestep" in settings)
    public void FixedUpdate()
    {
        timer += Time.fixedDeltaTime;
        if (timer > bulletDelay)
        {
            timer = 0f;
            HandleShootBullet.Invoke();

        }
    }
}
