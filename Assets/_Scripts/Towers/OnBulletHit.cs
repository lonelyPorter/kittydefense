﻿using UnityEngine;

/// <summary>
/// Destorys a Bullet if it hits a trigger
/// </summary>
public class OnBulletHit : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
