﻿using UnityEngine;

/// <summary>
/// Basic behaviour of each bullet.
/// This way we can set the force dependent on different bullets
/// and the bullet launcher (tower) does not need to know about this.
/// </summary>
public class BulletLaunch : MonoBehaviour
{
    //force with which the bullet is shot
    [SerializeField]
    private float force = 100f;

    [SerializeField]
    private int damage;

    //bullets "spawn" from tower-ParentObjects. Required for aiming the bullet.
    private GameObject towerParent;
    private GameObject closestEnemy;
    private Rigidbody rb;

    public int Damage { get => damage; set => damage = value; }

    /// <summary>
    /// When the bullet is spawned, it is launched at the closest Enemy.
    /// </summary>
    private void Awake()
    {
        towerParent = transform.parent.parent.gameObject;
        closestEnemy = towerParent.GetComponent<TowerFire>().GetTargetedEnemy();
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (closestEnemy)
            rb.velocity = (closestEnemy.transform.localPosition - transform.position) * force;
    }

}

