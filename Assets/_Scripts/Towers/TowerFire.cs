﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes a tower shoot according to its TowerBehaviour
/// </summary>
public class TowerFire : MonoBehaviour
{
    //Set SO according to kind of tower
    [SerializeField] SOTowerBehaviour towerBehaviour;

    [SerializeField] private LayerMask enemyLayer;

    private GameObject targetedEnemy = null;
    private float targetedEnemyDistance;
    private int skipCount = 0;

    private GameObject bullet;
    private Transform bulletSpawnPoint;

    private void Start()
    {
        bulletSpawnPoint = gameObject.transform.Find("BulletSpawnPoint");
        bullet = towerBehaviour.bullet;
        bullet.GetComponent<BulletLaunch>().Damage = towerBehaviour.bulletDamage;
    }
    private void OnEnable()
    {
        TowerTimer.HandleShootBullet += OnTowerActivity;
    }

    private void OnDisable()
    {
        TowerTimer.HandleShootBullet -= OnTowerActivity;
    }

    public GameObject GetTargetedEnemy()
    {
        return targetedEnemy;
    }

    private void TargetFarestEnemyInRange()
    {
        RaycastHit[] hits = Physics.SphereCastAll(gameObject.transform.position, towerBehaviour.range, transform.forward, towerBehaviour.range, enemyLayer, QueryTriggerInteraction.Collide);
        //Debug.Log(hits.Length + " Enemies in Range");

        GameObject farestEnemyInRange = null;
        float shortestDistance = Mathf.Infinity;

        foreach (RaycastHit hit in hits)
        {
            EnemyMove enemy = hit.transform.gameObject.GetComponentInParent<EnemyMove>();
            float distance = enemy.GetDistanceToTarget();

            if (shortestDistance > distance)
            {
                farestEnemyInRange = enemy.gameObject;
                shortestDistance = distance;
            }
        }

        targetedEnemy = farestEnemyInRange;
        targetedEnemyDistance = targetedEnemy ? Vector3.Distance(transform.position, targetedEnemy.transform.position) : Mathf.Infinity;
    }

    private void TargetClosestEnemy()
    {
        List<GameObject> enemies = new List<GameObject>();

        foreach (EnemyMove item in GameObject.FindObjectsOfType<EnemyMove>())
        {
            enemies.Add(item.gameObject);
        }

        float shortestDist = Mathf.Infinity;

        foreach (GameObject enemy in enemies)
        {
            float dist = Vector3.Distance(transform.position, enemy.transform.position);

            if (shortestDist > dist)
            {
                shortestDist = dist;
                targetedEnemy = enemy;
                targetedEnemyDistance = shortestDist;
            }
        }
    }

    private void OnTowerActivity()
    {
        if (skipCount <= towerBehaviour.bulletDelay / TowerTimer.bulletDelay)
        {
            skipCount++;
        }
        else
        {
            //TargetEnemyUpdate();
            TargetFarestEnemyInRange();

            if (EnemyTargeted() && InRange())
            {
                Destroy(Instantiate(bullet, bulletSpawnPoint.transform.position, Quaternion.identity, bulletSpawnPoint.transform), 2f);
                skipCount = 0;
            }
        }
    }

    private bool InRange()
    {
        if (targetedEnemyDistance <= towerBehaviour.range)
        {
            return true;
        }
        return false;
    }

    private bool EnemyTargeted()
    {
        if (targetedEnemy != null)
        {
            return true;
        }
        return false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(gameObject.transform.position, towerBehaviour.range);
    }
}