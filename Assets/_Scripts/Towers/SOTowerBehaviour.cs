﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tower Behaviour", menuName = "Tower Behavior")]
public class SOTowerBehaviour : ScriptableObject
{

    //what bullets does the Tower launch
    public GameObject bullet;

    //How much damage the bullet causes
    public int bulletDamage;

    //bullet is shot every <n>seconds  
    // only 2 or 3 seconds are available right now, see TowerTimer.cs
    public float bulletDelay;

    //different towers have different names
    public string TowerName;

    //different towers have different descriptions
    public string description;

    //defines max range of tower
    public float range;

    //offset defines how far from the player this will be placed
    public float offset;

}
