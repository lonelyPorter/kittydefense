﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Shows the current wave and countdown for the next wave
/// </summary>
public class WaveUIManager : MonoBehaviour
{
    [SerializeField]
    Text waveText;
    int wave = 1;

    //Coundtdown
    bool countdown = false;
    float timeRemaining;

    private void Awake()
    {
        UpdateUI();
        if (waveText == null)
        {
            Debug.LogWarning("waveText in WaveUIManager not set");
        }
    }

    private void OnEnable()
    {
        LevelLoader.HandleWaveIncrease += IncreaseWaveCount;
    }

    private void OnDisable()
    {
        LevelLoader.HandleWaveIncrease -= IncreaseWaveCount;
    }

    private void FixedUpdate()
    {
        if (countdown)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.fixedDeltaTime;
                waveText.text = "Next wave in " + (Mathf.Round(timeRemaining * 10f) / 10f) + "s";
            }
            else
            {
                wave++;
                UpdateUI();
                countdown = false;
            }

        }
    }

    private void IncreaseWaveCount(float delay)
    {
        countdown = true;
        timeRemaining = delay;

    }

    private void UpdateUI()
    {
        waveText.text = "Wave " + wave;
    }
}
