﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelect : MonoBehaviour
{
    [SerializeField]
    private GameObject levelSelectMenu;

    public void LoadLevelSelect()
    {
        gameObject.SetActive(false);
        levelSelectMenu.SetActive(true);
    }

    public void UnLoadLevelSelect()
    {
        levelSelectMenu.SetActive(false);
        gameObject.SetActive(true);
    }
}
