﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enables the VictoryMenu
/// </summary>
public class VictoryMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject victoryMenu;

    private void OnEnable()
    {
        LevelLoader.HandleVictory += EnableVictoryMenu;
    }

    private void OnDisable()
    {
        LevelLoader.HandleVictory -= EnableVictoryMenu;
    }

    private void EnableVictoryMenu()
    {
        victoryMenu.SetActive(true);
    }
}
