﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button muteButton;
    [SerializeField]
    private Button unMuteButton;
    [SerializeField]
    private AudioSource backgroundMusic;

    private void Start()
    {
        Time.timeScale = 1;
    }


    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }

    public void PlayTutorialLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }

    public void Mute()
    {
        muteButton.gameObject.SetActive(false);
        unMuteButton.gameObject.SetActive(true);
        backgroundMusic.Pause();
    }

    public void UnMute()
    {
        muteButton.gameObject.SetActive(true);
        unMuteButton.gameObject.SetActive(false);
        backgroundMusic.Play();
    }
}
