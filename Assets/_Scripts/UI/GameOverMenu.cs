﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Pauses the game and shows a gameOver-menu with options to restart or quit
/// </summary>
public class GameOverMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject gameOverMenu;

    private AudioManager audioManager;
    private HealthManager hm;
    [SerializeField]
    private string GameOverSoundName = "GameOver";

    private void Start()
    {
        audioManager = AudioManager.instance;

    }
    private void OnEnable()
    {
        hm = FindObjectOfType<HealthManager>();
        hm.HandleGameOver += LoadScreen;
    }

    private void OnDisable()
    {
        hm.HandleGameOver -= LoadScreen;
    }

    //sets Menu to active
    private void LoadScreen()
    {
        gameOverMenu.gameObject.SetActive(true);
        //sound that underlines the gameover
        audioManager.PlaySound(GameOverSoundName);
        Time.timeScale = 0f;
    }


    public void Restart()
    {
        Scene activeScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(activeScene.name, LoadSceneMode.Single);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void Quit()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
}
