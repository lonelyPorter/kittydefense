﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Shows the amount of kitties alive in the UI
/// </summary>
public class HealthUIManager : MonoBehaviour
{
    [SerializeField]
    private Text healthText;
    private HealthManager hm;

    private void Start()
    {
        if (healthText == null)
        {
            Debug.LogWarning("healthText in HealthUIManager not set");
        }
    }

    private void OnEnable()
    {
        hm = FindObjectOfType<HealthManager>();
        hm.HandleLivesChange += UpdateUI;
    }

    private void OnDisable()
    {
        hm.HandleLivesChange -= UpdateUI;
    }

    private void UpdateUI(int newLives)
    {
        healthText.text = "Lives: " + newLives;
    }
}
