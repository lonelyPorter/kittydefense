﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectLoader : MonoBehaviour
{
    [SerializeField]
    private int buildSettingsLevelIndex;

    [SerializeField]
    private AudioClip levelSelectSound;

    public void LoadLevel()
    {
        AudioSource.PlayClipAtPoint(levelSelectSound, Camera.main.transform.position);
        SceneManager.LoadScene(buildSettingsLevelIndex, LoadSceneMode.Single);
    }
}
