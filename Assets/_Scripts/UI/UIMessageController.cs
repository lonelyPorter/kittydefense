﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMessageController : MonoBehaviour
{
    [SerializeField]
    private GameObject textFieldParent;
    [SerializeField]
    private Text textBox;

    private GameStateManager gsm;
    private PlaceTower player;

    private void Awake()
    {
        gsm = FindObjectOfType<GameStateManager>();
        player = FindObjectOfType<PlaceTower>();
    }

    private void OnEnable()
    {
        gsm.HandleShowMessage += showMessage;
        player.HandleShowMessage += showMessage;
    }

    private void OnDisable()
    {
        gsm.HandleShowMessage -= showMessage;
        player.HandleShowMessage -= showMessage;
    }

    private void showMessage(string message)
    {
        textFieldParent.SetActive(true);
        textBox.text = message;

        StartCoroutine(hideMessage());
    }

    IEnumerator hideMessage()
    {
        yield return new WaitForSeconds(2f);
        textBox.text = "";
        textFieldParent.SetActive(false);
    }
}
