﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private GameObject credits;

    private bool isPlaying = false;

    public void PlayCredits()
    {
        //Shows the credits (unskippable), disables the play-Function until credits are over
        if (!isPlaying)
        {
            isPlaying = true;
            credits.SetActive(true);
            StartCoroutine(HideCredits());
        }
    }

    private IEnumerator HideCredits()
    {
        yield return new WaitForSeconds(13);
        credits.SetActive(false);
        isPlaying = false;
    }
}
