﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Pauses the Menu and shows options to continue, change settings or quit.
/// </summary>
public class PauseMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject breakMenu;

    private static bool gameIsPaused = false;

    private void Start()
    {
        Time.timeScale = 1;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void OpenMenuWithButton()
    {
        if (breakMenu.active)
        {
            Pause();
        }

    }

    void Resume()
    {
        breakMenu.gameObject.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    void Pause()
    {
        breakMenu.gameObject.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void ContinueGame()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            breakMenu.gameObject.SetActive(false);
        }
    }

    public void Reload()
    {
        Scene activeScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(activeScene.name, LoadSceneMode.Single);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
