﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Updates the Coin-Counter in the UI. Stores the current amount of coins.
/// </summary>
public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    int initialScore = 10;
    int score;
    [SerializeField]
    Text scoreText;

    private void OnEnable()
    {
        PlayerTriggerController.HandlePickup += IncreaseScore;
    }

    private void OnDisable()
    {
        PlayerTriggerController.HandlePickup -= IncreaseScore;
    }

    //Try to use Start() 
    private void Awake()
    {
        score = initialScore;

        if (scoreText != null)
        {
            UpdateScore();
        }
        else
        {
            Debug.LogError("scoreText in ScoreManager not set");
        }
    }

    private void IncreaseScore()
    {
        score++;
        UpdateScore();
    }

    private void UpdateScore()
    {
        scoreText.text = "Coins: " + score;
    }

    /// <summary>
    /// Decreases the number of coins of the player
    /// </summary>
    /// <param name="price"></param>
    /// <returns>true if sufficent coins and coins were subtracted, false if not</returns>
    public bool DecreaseScore(int price)
    {
        bool sufficentFunds = score >= price;
        if (sufficentFunds)
        {
            score = score - price;
            UpdateScore();
        }
        else
        {
            StartCoroutine(IndicateMissingFunds());
        }
        return sufficentFunds;
    }

    IEnumerator IndicateMissingFunds()
    {
        for (int i = 0; i < 2; i++)
        {
            scoreText.color = Color.red;
            yield return new WaitForSeconds(0.5f);
            scoreText.color = Color.white;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
