﻿using UnityEngine;
using UnityEngine.UI;

public class LevelSelectScoreLoader : MonoBehaviour
{
    [SerializeField]
    private int levelNumber;

    [SerializeField]
    private Sprite scoreSetSprite;
    [SerializeField]
    private Sprite scoreUnsetSprite;

    [SerializeField]
    private Image cat1;
    [SerializeField]
    private Image cat2;
    [SerializeField]
    private Image cat3;
    [SerializeField]
    private Image cat4;
    [SerializeField]
    private Image cat5;

    [SerializeField]
    private GameObject scoreHolder;

    void Start()
    {
        switch (HighScoreManager.instance.GetScoreForLevel(levelNumber))
        {
            case 0:
                scoreHolder.SetActive(false);
                break;
            case 1:
                scoreHolder.SetActive(true);
                cat1.sprite = scoreSetSprite;
                cat2.sprite = scoreUnsetSprite;
                cat3.sprite = scoreUnsetSprite;
                cat4.sprite = scoreUnsetSprite;
                cat5.sprite = scoreUnsetSprite;
                break;
            case 2:
                scoreHolder.SetActive(true);
                cat1.sprite = scoreSetSprite;
                cat2.sprite = scoreSetSprite;
                cat3.sprite = scoreUnsetSprite;
                cat4.sprite = scoreUnsetSprite;
                cat5.sprite = scoreUnsetSprite;
                break;
            case 3:
                scoreHolder.SetActive(true);
                cat1.sprite = scoreSetSprite;
                cat2.sprite = scoreSetSprite;
                cat3.sprite = scoreSetSprite;
                cat4.sprite = scoreUnsetSprite;
                cat5.sprite = scoreUnsetSprite;
                break;
            case 4:
                scoreHolder.SetActive(true);
                cat1.sprite = scoreSetSprite;
                cat2.sprite = scoreSetSprite;
                cat3.sprite = scoreSetSprite;
                cat4.sprite = scoreSetSprite;
                cat5.sprite = scoreUnsetSprite;
                break;
            case 5:
                scoreHolder.SetActive(true);
                cat1.sprite = scoreSetSprite;
                cat2.sprite = scoreSetSprite;
                cat3.sprite = scoreSetSprite;
                cat4.sprite = scoreSetSprite;
                cat5.sprite = scoreSetSprite;
                break;
        }


    }
}
