﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpController : MonoBehaviour
{
    private Animator animator;
    [SerializeField]
    private float despawnDelay = 20f;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        Invoke("Despawn", despawnDelay);
    }

    private void Despawn()
    {
        //Setting isDespawning to true so the animator plays the despawn clip (Duration: 1s)
        animator.SetBool("isDespawning", true);
        Destroy(this.gameObject, 1f);
    }

}
