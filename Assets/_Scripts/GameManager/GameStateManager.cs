﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The GameStateManager handles the States of the Games. Depending on the state of the game, certain
/// actions will be done.
/// </summary>
public class GameStateManager : MonoBehaviour
{
    private enum State { Preparing, Running, Victory, Lost };
    private State state = State.Preparing;

    //Stats to be displayed
    private int coinsCollected = 0;
    private int kills = 0;
    private int towers = 0;
    private int cats = 5;

    //to initialize particles as child of this GO
    private GameObject particleParent;

    [SerializeField]
    private Text startText;
    [SerializeField]
    private TextMeshProUGUI statsText;
    [SerializeField]
    private Text defeatText;

    [SerializeField]
    private GameObject confetti;

    // Audio related fields
    private AudioManager audioManager;
    private HealthManager healthManager;
    [SerializeField]
    private string victorySoundName = "Victory";

    public event Action<String> HandleShowMessage = delegate { };
    public static event Action HandleGameStart = delegate { };
    public static event Action HandleStartSpawning = delegate { };

    private void Start()
    {
        audioManager = AudioManager.instance;
        particleParent = GameObject.Find("Particles");
        if (particleParent == null)
            Debug.LogError("GameObject Particles not found");
    }

    private void Update()
    {
        //When the player presses Space in the preparation phase, the game will start
        if (state == State.Preparing && Input.GetKeyDown(KeyCode.Space))
        {
            EndPreparing();
            state = State.Running;
        }
    }

    private void OnDestroy()
    {
        HandleGameStart = delegate { };
        HandleStartSpawning = delegate { };
    }

    private void OnEnable()
    {
        Debug.Log("GameStateManager: Starting the Game");
        PlayerTriggerController.HandlePickup += AddCoin;
        EnemyHealthController.HandleAnyDeath += AddKill;
        PlaceTower.HandleTowerBuilt += AddTower;
        LevelLoader.HandleVictory += EndGameVictorious;
        SOPlayerLives.HandleGameOver += EndGameDefeated;
        healthManager = FindObjectOfType<HealthManager>();
        healthManager.HandleLivesChange += HealthChange;

        //Not used at the moment
        HandleGameStart?.Invoke();
    }

    private void HealthChange(int health)
    {
        cats = health;
    }

    private void OnDisable()
    {
        PlayerTriggerController.HandlePickup -= AddCoin;
        EnemyHealthController.HandleAnyDeath -= AddKill;
        PlaceTower.HandleTowerBuilt -= AddTower;
        LevelLoader.HandleVictory -= EndGameVictorious;
        SOPlayerLives.HandleGameOver -= EndGameDefeated;
        healthManager.HandleLivesChange -= HealthChange;
    }

    private void EndGameVictorious()
    {
        state = State.Victory;
        statsText.text = "\n" + coinsCollected +
                            "\n" + kills +
                            "\n" + towers;
        audioManager.PlaySound(victorySoundName);
        Instantiate(confetti, particleParent.transform.position, particleParent.transform.rotation, particleParent.transform);
        HighScoreManager.instance.SaveScoreIfImproved(cats);
    }

    private void EndGameDefeated()
    {
        state = State.Lost;
        defeatText.text = "Shame on you." +
                            "\nYour Stats:" +
                            "\nCoins collected:\t\t" + coinsCollected +
                            "\nEnemies Killed:\t\t" + kills +
                            "\nTowers placed:\t\t" + towers;
        defeatText.gameObject.SetActive(true);
    }

    private void AddCoin()
    {
        coinsCollected++;
    }

    private void AddKill()
    {
        kills++;
    }

    private void AddTower()
    {
        towers++;
    }

    private void EndPreparing()
    {
        HandleShowMessage?.Invoke("Let's do this!");
        HandleStartSpawning?.Invoke();
    }
}
