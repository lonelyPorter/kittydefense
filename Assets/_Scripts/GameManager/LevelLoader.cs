﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Reads SOWaves and tells the EnemySpawner to spawn the correct enemies at the correct time.
/// </summary>
public class LevelLoader : MonoBehaviour
{
    private float timer = 0f;

    private bool spawnStarted = false;

    //Waves
    [SerializeField]
    private SOWave[] waves;
    SOWave currentWave;
    int numberOfWaves;
    int currentWaveCount;
    int enemiesLeft;

    //Groups (A wave consists of multiple Groups)
    EnemyGroup currentGroup;
    int numberOfGroups;
    int currentGroupCount;
    int enemiesLeftInGroup;

    float nextDelay;

    public static event Action<GameObject> HandleSpawnEnemy = delegate { };
    public static event Action<float> HandleWaveIncrease = delegate { };
    public static event Action HandleVictory = delegate { };

    private void Awake()
    {
        numberOfWaves = waves.Length;
        currentWaveCount = 1;
        LoadNextWave();
    }

    private void OnEnable()
    {
        EnemyHealthController.HandleAnyDeath += DecreaseEnemyCount;
        GameStateManager.HandleStartSpawning += StartSpawning;
    }

    private void OnDisable()
    {
        EnemyHealthController.HandleAnyDeath -= DecreaseEnemyCount;
        GameStateManager.HandleStartSpawning -= StartSpawning;
    }

    private void OnDestroy()
    {
        HandleSpawnEnemy = delegate { };
        HandleWaveIncrease = delegate { };
        HandleVictory = delegate { };
    }

    private void StartSpawning()
    {
        spawnStarted = true;
    }

    private void DecreaseEnemyCount()
    {
        enemiesLeft--;
        if (enemiesLeft == 0)
        {
            if (currentWaveCount < numberOfWaves)
            {
                Debug.Log("Next Wave starts in " + currentGroup.PauseDelayAfterGroup + " seconds");
                HandleWaveIncrease?.Invoke(currentGroup.PauseDelayAfterGroup);
            }
            else
            {
                HandleVictory?.Invoke();
            }
        }
    }

    private void FixedUpdate()
    {
        timer += Time.fixedDeltaTime;

        if (spawnStarted && timer > nextDelay)
        {
            timer = 0f;

            //Spawn enemies of this group until no enemies left
            if (enemiesLeftInGroup > 0)
            {
                if (enemiesLeftInGroup == currentGroup.NumberOfEnemies)
                    nextDelay = currentGroup.DelayBetweenSpawns;
                HandleSpawnEnemy.Invoke(currentGroup.EnemyType);
                enemiesLeftInGroup--;
            }
            // Set Group to next group
            else if (currentGroupCount < numberOfGroups)
            {
                currentGroupCount++;
                LoadNextGroup();
            }
            // Load next wave
            else if (currentWaveCount < numberOfWaves && enemiesLeft == 0)
            {
                currentWaveCount++;
                LoadNextWave();
            }
        }
    }

    private void LoadNextGroup()
    {
        //Setting the Delay before loading the next group (except at the start)
        if (currentGroup != null)
            nextDelay = currentGroup.PauseDelayAfterGroup;

        currentGroup = currentWave.GetGroups()[currentGroupCount - 1];
        enemiesLeftInGroup = currentGroup.NumberOfEnemies;
        Debug.Log("Starting Group " + currentGroupCount + "/" + numberOfGroups);
    }

    private void LoadNextWave()
    {
        currentWave = waves[currentWaveCount - 1];
        currentGroupCount = 1;
        numberOfGroups = currentWave.GetGroups().Length;
        enemiesLeft = 0; // Should be 0 anyways
        foreach (EnemyGroup group in currentWave.GetGroups())
        {
            enemiesLeft += group.NumberOfEnemies;
        }
        Debug.Log("Starting Wave " + currentWaveCount + "/" + numberOfWaves);

        LoadNextGroup();
    }
}
