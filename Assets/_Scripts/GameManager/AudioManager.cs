﻿using UnityEngine;

/// <summary>
/// Sound Class that the AudioManager uses.
/// Holds an audioclip, name, colume and pitch
/// </summary>
[System.Serializable]
public class Sound
{
    [SerializeField]
    private string name;
    [SerializeField]
    private AudioClip audioClip;

    private AudioSource source;

    [SerializeField]
    [Range(0f, 1f)]
    private float volume = 1f;
    [SerializeField]
    [Range(0.5f, 1.5f)]
    private float pitch = 1f;

    public string Name { get => name; set => name = value; }

    public AudioSource Source
    {
        private get { return source; }
        set
        {
            source = value;
            source.clip = audioClip;
        }
    }

    public void Play()
    {
        source.volume = volume;
        source.pitch = pitch;
        source.Play();
    }
}

public class AudioManager : MonoBehaviour
{
    //Singleton instance
    public static AudioManager instance;

    //Empty Gameobject in the scene as a parent for organizing
    [SerializeField]
    private GameObject soundParent;

    //Holds all the possible sounds of the game
    [SerializeField]
    private Sound[] sounds;

    private void Awake()
    {
        //Singleton Instance AudioManager should only exist once per scene
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        //Instantiate Gameobjects for every AudioClip
        for (int i = 0; i < sounds.Length; i++)
        {
            GameObject go = new GameObject("Sound_" + i + "_" + sounds[i].Name);
            go.transform.SetParent(soundParent.transform);
            sounds[i].Source = go.AddComponent<AudioSource>();
        }
    }

    /// <summary>
    /// Plays the sound with the given Name
    /// </summary>
    /// <param name="name">Name of the sound</param>
    public void PlaySound(string name)
    {
        foreach (Sound sound in sounds)
        {
            if (sound.Name.Equals(name))
            {
                sound.Play();
                return;
            }
        }

        Debug.LogWarning("AudioManager: Could not find sound with name '" + name + "'");
    }
}
