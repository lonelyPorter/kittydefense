﻿using UnityEngine;

/// <summary>
/// A Level can be finished with a score between 1 and 5.
/// 5 meaning all Cats have survived, 1 meaning only one survived.
/// </summary>
public class HighScoreManager : MonoBehaviour
{

    public static HighScoreManager instance;

    [SerializeField] private int level;

    private void Awake()
    {
        //Singleton Instance HighScoreManager should only exist once per scene
        if (instance == null)
        {
            instance = this;
        }
    }

    public void ResetHighScore()
    {
        PlayerPrefs.SetInt("level0Score", 0);
        PlayerPrefs.SetInt("level1Score", 0);
        PlayerPrefs.SetInt("level2Score", 0);
        PlayerPrefs.SetInt("level3Score", 0);
        PlayerPrefs.SetInt("level4Score", 0);
        PlayerPrefs.SetInt("level5Score", 0);
        PlayerPrefs.SetInt("level6Score", 0);
        PlayerPrefs.SetInt("level7Score", 0);
    }

    /// <summary>
    /// Saves the score of the current level
    /// </summary>
    /// <param name="score"></param>
    public void SaveScoreIfImproved(int score)
    {
        SaveScoreIfImproved(level, score);
    }

    public void SaveScoreIfImproved(int level, int score)
    {
        if (level < 0 || level > 7)
            return;

        PlayerPrefs.SetInt("level" + level + "Score", score);
        Debug.Log("Saved Score of [" + score + "] for level " + level);
    }

    public int GetScoreForLevel(int level)
    {
        int score = PlayerPrefs.GetInt("level" + level + "Score");
        Debug.Log("Loaded Score of [" + score + "] for level " + level);

        return score;
    }
}
