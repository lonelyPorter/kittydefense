﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyGroup
{
    [SerializeField]
    private GameObject enemyType;
    [SerializeField]
    private int numberOfEnemies;
    [SerializeField]
    private float delayBetweenSpawns;
    [SerializeField]
    private float pauseDelayAfterGroup;

    public GameObject EnemyType { get => enemyType; }
    public int NumberOfEnemies { get => numberOfEnemies; }
    public float DelayBetweenSpawns { get => delayBetweenSpawns; }
    public float PauseDelayAfterGroup { get => pauseDelayAfterGroup; }


    public GameObject GetEnemyType()
    {
        return enemyType;
    }
}

[CreateAssetMenu(fileName = "New Wave", menuName = "Wave")]
public class SOWave : ScriptableObject
{
    [SerializeField]
    EnemyGroup[] groups;

    public EnemyGroup[] GetGroups()
    {
        return groups;
    }
}
