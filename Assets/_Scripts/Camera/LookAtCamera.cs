﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes an object always look at the Camera.
/// Uses the Main-Camera where the AudioListener is on
/// </summary>
public class LookAtCamera : MonoBehaviour
{
    private Transform cameraTransform;

    private void Awake()
    {
        //Searching Camera in Scene (The AudioListener is always unique)
        cameraTransform = FindObjectOfType<AudioListener>().transform;
    }

    void LateUpdate()
    {
        transform.LookAt(transform.position + cameraTransform.forward);
    }
}
