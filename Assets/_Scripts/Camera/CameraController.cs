﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// CameraController can be added to a Camera. Cameras with this Controller
/// will always follow the Player
/// </summary>
public class CameraController : MonoBehaviour
{
    private GameObject player;
    private Vector3 offset;

    private void Awake()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
    }

    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}
