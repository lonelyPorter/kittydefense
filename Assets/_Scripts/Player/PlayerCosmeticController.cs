﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A Class to Enable and Disable Cosmetics for the Player
/// </summary>
public class PlayerCosmeticController : MonoBehaviour
{
    [SerializeField]
    private GameObject cosmeticHat;
    [SerializeField]
    private GameObject cosmeticPants;
    public void EnableHat()
    {
        cosmeticHat.SetActive(true);
    }

    public void DisableHat()
    {
        cosmeticHat.SetActive(false);
    }

    public void EnablePants()
    {
        cosmeticPants.SetActive(true);
    }

    public void DisablePants()
    {
        cosmeticPants.SetActive(false);
    }
}
