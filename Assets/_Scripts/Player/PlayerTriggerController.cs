﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enables the Player to interact with Triggers, for example PickUps
/// </summary>
public class PlayerTriggerController : MonoBehaviour
{
    public static event Action HandlePickup = delegate { };

    // Audio related fields
    private AudioManager audioManager;
    [SerializeField]
    private string pickUpSoundName = "pickUp";

    private void Start()
    {
        //Getting AudioManager instance, this has to be done in Start, because in Awake, the instance is not ready
        audioManager = AudioManager.instance;
    }
    private void OnDestroy()
    {
        HandlePickup = delegate { };
    }

    private void OnTriggerEnter(Collider other)
    {
        // The Collider is always in the Child of the Geometry-Child, so the GameObject is grandparent.
        GameObject grandparent = other.transform.parent.parent.gameObject;

        if (grandparent == null)
        {
            Debug.LogWarning("Collider of " + other.gameObject.name + " should be in 'Parent-GameObject/Geometry/GameObject with Collider'");
        }
        else if (grandparent.CompareTag("Pick Up"))
        {
            grandparent.tag = "Picked Up";
            audioManager.PlaySound(pickUpSoundName);
            grandparent.GetComponentInChildren<Animator>().SetBool("isPickedUp", true);
            Destroy(grandparent, 1f);
            HandlePickup?.Invoke();
        }
    }
}
