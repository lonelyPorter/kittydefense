﻿using System;
using UnityEngine;

/// <summary>
/// Handles Collisions for the Enemy
/// Invokes HandleCollision when hit
/// </summary>
public class EnemyCollisionController : MonoBehaviour
{
    public event Action<int> HandleCollision = delegate { };


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            //Marking the shot used (because Destroying takes longer than a Frame and Destroying the shot
            other.gameObject.tag = "BulletUsed";
            int dmg = other.GetComponentInParent<BulletLaunch>().Damage;

            //Messaging everyone that cares about the collision
            HandleCollision?.Invoke(dmg);
        }
    }

}
