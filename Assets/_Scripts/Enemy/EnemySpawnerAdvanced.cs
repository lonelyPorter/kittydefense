﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerAdvanced : EnemySpawner
{
    // Location of the spawned Enemy
    [SerializeField]
    private Transform spawnPoint2;

    private bool spawn2 = true;

    protected override void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy,
            spawn2 ? spawnPoint2.position : spawnPoint.position,
            spawn2 ? spawnPoint2.rotation : spawnPoint.rotation,
            enemyParent.transform);

        spawn2 = !spawn2;
    }
}
