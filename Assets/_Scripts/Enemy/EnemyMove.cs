﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Uses Unitys NavMesh-System to make an enemy move to a destination
/// </summary>
public class EnemyMove : MonoBehaviour
{
    [SerializeField]
    Transform destination;

    NavMeshAgent agent;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();

        SetDestination();
    }

    private void SetDestination()
    {
        destination = GameObject.Find("Destination").transform;
        if (destination != null)
        {
            //Telling the Agent to walk to the desired position
            agent.SetDestination(destination.transform.position);

        }
        else
        {
            Debug.LogError("Destination Object not found");
        }
    }

    //https://stackoverflow.com/questions/61421172/why-does-navmeshagent-remainingdistance-return-values-of-infinity-and-then-a-flo
    public float GetDistanceToTarget()
    {
        if (agent.pathPending ||
            agent.pathStatus == NavMeshPathStatus.PathInvalid ||
            agent.path.corners.Length == 0)
            return -1f;

        float distance = 0.0f;
        for (int i = 0; i < agent.path.corners.Length - 1; ++i)
        {
            distance += Vector3.Distance(agent.path.corners[i], agent.path.corners[i + 1]);
        }

        return distance;
    }
}
