﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A HealthBar using a slider to fill a Gradient.
/// Uses LeanTween for extra juicieness
/// </summary>
public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Slider slider;
    [SerializeField]
    private Gradient gradient;
    [SerializeField]
    private Image fill;

    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    public void SetHealthValue(int health)
    {
        float startValue = slider.value;
        float endValue = health;
        float timeToComplete = 0.5f;

        // tween value from startValue to endValue in timeToComplete
        LeanTween.value(gameObject, startValue, endValue, timeToComplete)
            .setEase(LeanTweenType.easeOutSine)
            .setOnUpdate((float currentValue) =>
            {
                slider.value = currentValue;
                fill.color = gradient.Evaluate(slider.normalizedValue);
            });
    }

    public void SetMaxHealthValue(int health)
    {
        slider.maxValue = health;
        slider.value = health;
        fill.color = gradient.Evaluate(1f);
    }
}
