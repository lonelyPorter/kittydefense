﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible for the health of the Controller
/// Invokes DeathController Events when health gets zero
/// </summary>
public class EnemyHealthController : MonoBehaviour
{
    [SerializeField]
    private int health = 2;
    private HealthBar healthBar;

    private EnemyCollisionController cc;

    // Audio related fields
    private AudioManager audioManager;
    [SerializeField]
    private string hitSoundName = "EnemyHit";

    public event Action HandleDeath = delegate { };
    public event Action HandleDestroy = delegate { };
    public static event Action HandleAnyDeath = delegate { };

    private void Awake()
    {
        //Getting the CollisionController Script from the Enemy
        cc = GetComponent<EnemyCollisionController>();
        healthBar = GetComponentInChildren<HealthBar>();
        healthBar.SetMaxHealthValue(health);
        audioManager = AudioManager.instance;
    }
    private void OnDestroy()
    {
        // This was stated to prevent errors that occured after building the game. However, this was not the case here. Emptying the delegate will remove
        // the delegates of the living enemies as well, leading to misbehaviour. It was therefore commented out again.
        // HandleAnyDeath = delegate { };
    }

    private void OnEnable()
    {
        cc.HandleCollision += GetDamage;
    }

    private void OnDisable()
    {
        cc.HandleCollision -= GetDamage;
    }

    private void GetDamage(int dmg)
    {
        health -= dmg;
        healthBar.SetHealthValue(health);

        if (health <= 0)
        {
            HandleDeath?.Invoke();
            HandleAnyDeath?.Invoke();

        }
        else
        {
            // Play hit sound and/or animation
            audioManager.PlaySound(hitSoundName);
        }
    }

    public void DestroyEnemy()
    {
        HandleAnyDeath?.Invoke();
        HandleDestroy?.Invoke();
    }
}
