﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible for all actions, that should be made after an enemy dies.
/// For example playing an animation, playing a sound or dropping loot.
/// </summary>
public class EnemyDeathController : MonoBehaviour
{
    private EnemyHealthController hc;

    // Audio related fields
    private AudioManager audioManager;
    [SerializeField]
    private string deathSoundName = "EnemyDies";

    //Drops
    [SerializeField]
    private GameObject drop;
    [SerializeField]
    [Range(0, 4)]
    private int dropAmount = 1;
    private GameObject parent;
    private GameObject particleParent;

    //Particles
    [SerializeField]
    GameObject deathParticles;

    private void Awake()
    {
        hc = GetComponent<EnemyHealthController>();
        parent = GameObject.Find("PickUps");
        particleParent = GameObject.Find("Particles");
        if (parent == null)
        {
            Debug.LogError("GameObject PickUps not found");
        }
        if (particleParent == null)
        {
            Debug.LogError("GameObject Particles not found");
        }
    }

    private void Start()
    {
        audioManager = AudioManager.instance;
    }

    private void OnEnable()
    {
        hc.HandleDeath += Die;
        hc.HandleDestroy += DestroyEnemy;
    }

    private void OnDisable()
    {
        hc.HandleDeath -= Die;
        hc.HandleDestroy -= DestroyEnemy;
    }

    /// <summary>
    /// Destroys the Enemy after playing its death-Sound and dropping its loot
    /// </summary>
    private void Die()
    {
        Destroy(this.gameObject);
        audioManager.PlaySound(deathSoundName);
        DropLoot();

        if (particleParent == null)
        {
            particleParent = new GameObject("ParticleParent");
        }

        Destroy(Instantiate(deathParticles, transform.position, transform.rotation, particleParent.transform), 0.35f);
    }

    /// <summary>
    /// Destorys an Enemy without dropping loot or playing a sound
    /// </summary>
    private void DestroyEnemy()
    {
        Destroy(this.gameObject);
    }

    /// <summary>
    ///  Drops between 1 and 4 coins relative to the point where the enemy died
    /// </summary>
    private void DropLoot()
    {
        Transform enemyTransform = gameObject.transform;
        // Coins should always be 0.5 above ground (otherwise they can get stuck under the scene...)
        enemyTransform.position = new Vector3(enemyTransform.position.x, 1.5f, enemyTransform.position.z);
        switch (dropAmount)
        {
            case 1:
                Instantiate(drop, enemyTransform.position, enemyTransform.rotation, parent.transform);
                break;
            case 2:
                DropTwo(enemyTransform);
                break;
            case 3:
                DropThree(enemyTransform);
                break;
            case 4:
                DropFour(enemyTransform);
                break;
        }

    }

    private void DropTwo(Transform enemyTransform)
    {
        Instantiate(drop, enemyTransform.position + Vector3.left * 0.3f, enemyTransform.rotation, parent.transform);
        Instantiate(drop, enemyTransform.position + Vector3.right * 0.3f, enemyTransform.rotation, parent.transform);
    }

    private void DropThree(Transform enemyTransform)
    {
        DropTwo(enemyTransform);
        Instantiate(drop, enemyTransform.position + Vector3.forward * 0.3f, enemyTransform.rotation, parent.transform);
    }

    private void DropFour(Transform enemyTransform)
    {
        DropThree(enemyTransform);
        Instantiate(drop, enemyTransform.position + Vector3.back * 0.3f, enemyTransform.rotation, parent.transform);
    }
}
