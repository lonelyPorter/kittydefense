﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Subscribes to the LevelLoader.
/// Spawns an enemy whenever the levelloader invokes the event
/// </summary>
public class EnemySpawner : MonoBehaviour
{
    // Location of the spawned Enemy
    [SerializeField]
    protected Transform spawnPoint;

    // Parent of the new enemies in the Scene
    protected GameObject enemyParent;

    private void Awake()
    {
        enemyParent = new GameObject("Enemies");
    }

    private void OnEnable()
    {
        LevelLoader.HandleSpawnEnemy += SpawnEnemy;
    }

    private void OnDisable()
    {
        LevelLoader.HandleSpawnEnemy -= SpawnEnemy;
    }

    // Spawns an enemy at the spawnPoint with the enemyParent as parent
    protected virtual void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation, enemyParent.transform);
    }
}
