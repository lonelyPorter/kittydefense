﻿using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField]
    public GameObject[] tutorialInstructions;
    private int instructionIndex = 0;
    private bool towerBuilt;
    private bool startSponing;


    private void OnEnable()
    {
        PlaceTower.HandleTowerBuilt += IncreseTowers;
        GameStateManager.HandleStartSpawning += SetStartSpawning;

    }
    private void OnDisable()
    {
        PlaceTower.HandleTowerBuilt -= IncreseTowers;
        GameStateManager.HandleStartSpawning -= SetStartSpawning;
    }
    // Update is called once per frame
    void Update()
    {
        switch (instructionIndex)
        {
            //learn to walk
            case 0:
                tutorialInstructions[0].SetActive(true);
                float movementX = Input.GetAxis("Horizontal");
                float movementZ = Input.GetAxis("Vertical");
                if (movementX != 0f || movementZ != 0f)
                {
                    IncreaseIndex();

                }
                break;
            //learn how to open the towermenu
            case 1:
                if (Input.GetKeyDown(KeyCode.T))
                {
                    IncreaseIndex();
                }
                break;
            //learn to activate the preview
            case 2:
                if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha2))
                {
                    IncreaseIndex();
                }
                break;
            //learn how to build a tower
            case 3:
                if (towerBuilt)
                {
                    IncreaseIndex();
                }
                break;
            //learn how to start a Wave
            case 4:
                if (startSponing)
                {
                    IncreaseIndex();
                }
                break;
            //wish luck mabye remove later
            case 5:
                break;

            default:
                break;
        }

    }
    
    void IncreaseIndex()
    {
        tutorialInstructions[instructionIndex].SetActive(false);
        instructionIndex++;
        Debug.Log("Increase Index" + instructionIndex);
        tutorialInstructions[instructionIndex].SetActive(true);
        
    }

    void IncreseTowers()
    {
        towerBuilt= true;
    }

 void SetStartSpawning()
    {
        startSponing = true;
    }
}
