﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is a Script for a collectable egg. When the first egg is collected,
/// it gets set inactive and the next hidden egg will be enabled
/// </summary>
public class Egg : MonoBehaviour
{

    [SerializeField]
    GameObject next;
    private PlayerCosmeticController pcc;

    private void Awake()
    {
        pcc = FindObjectOfType<PlayerCosmeticController>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        gameObject.SetActive(false);
        if (next != null)
        {
            next.SetActive(true);
        }
        else
        {
            //Easteregg complete, Trigger Cat-Ears for Player
            pcc.EnableHat();
            pcc.EnablePants();
        }
    }
}
