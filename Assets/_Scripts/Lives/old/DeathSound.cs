﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSound : MonoBehaviour
{
    private AudioManager audioManager;
    [SerializeField]
    private string splatSoundName = "CatDies";

    private void Start()
    {
        audioManager = AudioManager.instance;
    }
    private void OnEnable()
    {
        SOPlayerLives.MakeDeathSound += MakeDeathSound;
    }

    private void OnDisable()
    {
        SOPlayerLives.MakeDeathSound -= MakeDeathSound;
    }

    private void MakeDeathSound()
    {
        audioManager.PlaySound(splatSoundName);
    }
}
