﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesManager : MonoBehaviour
{
    [SerializeField] Text playerHealth;
    [SerializeField] SOPlayerLives _pl;

    private void Start()
    {
        Debug.Log("Hiiii");
        //this is where the initial health is set       
        playerHealth.text = "player lives " + _pl.GetLives().ToString();
    }
    private void OnEnable()
    {
        SOPlayerLives.HandlePlayerLifeReduction += ChangePlayerLivesDisplay;
    }

    private void OnDisable()
    {
        SOPlayerLives.HandlePlayerLifeReduction -= ChangePlayerLivesDisplay;
    }

    private void ChangePlayerLivesDisplay()
    {

        playerHealth.text = "Lives: " + _pl.GetLives().ToString();
    }
}
