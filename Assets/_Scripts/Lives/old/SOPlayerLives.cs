﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "New PlayerLives", menuName = "PlayerLives")]
public class SOPlayerLives : ScriptableObject
{
    //This SO holds information about the amount of lives the player is given

    private int maxLives = 5;
    public int runtimeLives;


    Stack<GameObject> lifeRepresentations = new Stack<GameObject>();

    public static event Action HandlePlayerLifeReduction = delegate { };
    public static event Action HandleGameOver = delegate { };
    public static event Action MakeDeathSound = delegate { };


    private void OnEnable()
    {
        //audioManager = AudioManager.instance;
        //EnemyEnters.HandleEnemyEntersTrigger += GetDamage;
        //GameStateManager.HandleGameStart += ResetLives;
        //GameStateManager.HandleGameStart += InitializeLifeRepresentations;
        //InitializeLifeRepresentations();
    }

    private void OnDisable()
    {
        //EnemyEnters.HandleEnemyEntersTrigger -= GetDamage;
        //GameStateManager.HandleGameStart -= ResetLives;
        //GameStateManager.HandleGameStart -= InitializeLifeRepresentations;
    }

    private void OnDestroy()
    {
        HandlePlayerLifeReduction = delegate { };
        HandleGameOver = delegate { };
        MakeDeathSound = delegate { };
    }

    private void GetDamage()
    {
        if (runtimeLives > 0)
        {
            Debug.Log("A poor Kitty has been killed in action");
            runtimeLives--;
            GameObject go = lifeRepresentations.Pop();
            // the blood puddle-model is set active while the cat-model is set inactive
            go.transform.GetChild(0).gameObject.SetActive(true);
            go.transform.GetChild(1).gameObject.SetActive(false);
            HandlePlayerLifeReduction.Invoke();
            MakeDeathSound.Invoke();
        }
        else
        {
            // this separate invoke is nevessary, otherwise the last life would make no sound upon disappearing
            MakeDeathSound.Invoke();

            //When player has no more lives left, the game is over.
            HandleGameOver.Invoke();
        }

    }

    public int GetLives()
    {
        // Debug.Log("GetLives called "+runtimeLives);      
        return this.runtimeLives;
    }

    public void ResetLives()
    {
        this.runtimeLives = this.maxLives;
        Debug.Log("reset lives " + runtimeLives);
    }

    public void InitializeLifeRepresentations()
    {
        Debug.Log("InitializeLifeRepresentations");
        FindonlyLife[] life = GameObject.FindObjectsOfType<FindonlyLife>();

        foreach (FindonlyLife item in life)
        {
            lifeRepresentations.Push(item.gameObject);

        }

    }

}
