﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the Health of the player, visualized in 5 Kitties sitting in the ground
/// Calls UIManager when a kitty is killed and "kills" kitties in a pool of blood
/// Also Invokes GameOver-Screen when no kitty is left
/// </summary>
public class HealthManager : MonoBehaviour
{
    [SerializeField]
    private int maxHealth = 5;
    private int currentHealth;
    private AudioManager audioManager;

    private Stack<GameObject> lifeRepresentations = new Stack<GameObject>();

    [SerializeField]
    private string splatSoundName = "CatDies";

    public event Action<int> HandleLivesChange = delegate { };
    public event Action HandleGameOver = delegate { };

    private void Start()
    {
        currentHealth = maxHealth;
        HandleLivesChange.Invoke(currentHealth);
        audioManager = AudioManager.instance;
        InitializeLifeRepresentations();
    }

    private void OnEnable()
    {
        EnemyEnters.HandleEnemyEntersTrigger += GetDamage;
    }

    private void OnDisable()
    {
        EnemyEnters.HandleEnemyEntersTrigger -= GetDamage;
    }

    private void OnDestroy()
    {
        HandleLivesChange = delegate { };
        HandleGameOver = delegate { };
    }

    private void GetDamage()
    {
        currentHealth--;
        audioManager.PlaySound(splatSoundName);
        if (currentHealth >= 0)
        {
            Debug.Log("A poor Kitty has been killed in action");
            GameObject go = lifeRepresentations.Pop();
            // the blood puddle-model is set active while the cat-model is set inactive
            go.transform.GetChild(0).gameObject.SetActive(true);
            go.transform.GetChild(1).gameObject.SetActive(false);
            HandleLivesChange.Invoke(currentHealth);
        }

        if (currentHealth == 0)
        {
            //When player has no more lives left, the game is over.
            HandleGameOver.Invoke();
        }
    }

    private void InitializeLifeRepresentations()
    {
        Debug.Log("InitializeLifeRepresentations");
        FindonlyLife[] life = GameObject.FindObjectsOfType<FindonlyLife>();

        foreach (FindonlyLife item in life)
        {
            lifeRepresentations.Push(item.gameObject);

        }
    }
}
