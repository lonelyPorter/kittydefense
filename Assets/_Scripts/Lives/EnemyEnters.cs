﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Triggers the cats to begin meowing in fear
/// </summary>
public class EnemyEnters : MonoBehaviour
{
    public static event Action HandleEnemyEntersTrigger = delegate { };

    private void OnDestroy()
    {
        HandleEnemyEntersTrigger = delegate { };
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            //Marking the entering Enemy as mitigated so it won't cost more than one life
            other.gameObject.tag = "EnemyMitigated";

            HandleEnemyEntersTrigger?.Invoke();
            //Destorying the Enemy via it's Health Controller so the LevelLoader knows it's been destoryed
            other.transform.parent.parent.GetComponent<EnemyHealthController>().DestroyEnemy();
        }
    }
}
