﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Causes the cat to meow and tremble using insideUnitSphere
/// </summary>
public class Trembling : MonoBehaviour
{
    [SerializeField]
    float tremblingDuration = 2;

    private bool shake = false;
    private float timeRemaining;
    private float shakeIntensity = 0.05f;

    // Audio related fields
    private AudioManager audioManager;
    [SerializeField]
    private string meowSoundName = "Meow";

    private void Start()
    {
        audioManager = AudioManager.instance;
    }

    private void OnEnable()
    {
        CauseTrembling.HandleEnemiesNear += TremblingLifeRepresentation;
    }

    private void OnDisable()
    {
        CauseTrembling.HandleEnemiesNear -= TremblingLifeRepresentation;
    }

    private void FixedUpdate()
    {
        if (shake)
        {
            if (timeRemaining > 0)
            {
                this.transform.localPosition = Random.insideUnitSphere * shakeIntensity;
                timeRemaining -= Time.fixedDeltaTime;
            }
            else
            {
                shake = false;
            }
        }
    }

    private void TremblingLifeRepresentation()
    {
        shake = true;
        timeRemaining = tremblingDuration;
        Meow();
    }

    private void Meow()
    {
        audioManager.PlaySound(meowSoundName);
    }
}
