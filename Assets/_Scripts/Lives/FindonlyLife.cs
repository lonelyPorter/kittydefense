﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this script's only purpose is to enable other scripts to search for:
/// GameObject.FindObjectsOfType<FindonlyLife>
/// </summary>
public class FindonlyLife : MonoBehaviour
{

}
