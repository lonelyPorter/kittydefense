﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Causes the representation of player lives to "tremble" when enemies are near
/// </summary>
public class CauseTrembling : MonoBehaviour
{
    public static event Action HandleEnemiesNear = delegate { };
    private void OnTriggerEnter(Collider other)
    {
        //EnemyMove em = other.transform.parent.parent.GetComponent<EnemyMove>();
        if (other.CompareTag("Enemy"))
        //if (em != null)
        {
            HandleEnemiesNear.Invoke();
        }

    }
    private void OnDestroy()
    {
        //HandleEnemiesNear = delegate { };
    }
}
