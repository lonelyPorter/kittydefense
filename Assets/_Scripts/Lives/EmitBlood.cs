﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawns Blood Particles when a cat is killed
/// </summary>
public class EmitBlood : MonoBehaviour
{
    [SerializeField] GameObject bloodParticles;

    private GameObject particleParent;
    private HealthManager hm;

    private void Awake()
    {
        particleParent = GameObject.Find("Particles");
        if (particleParent == null)
        {
            Debug.LogError("GameObject Particles not found");
        }
    }

    private void OnEnable()
    {
        hm = FindObjectOfType<HealthManager>();
        hm.HandleLivesChange += BeginParticles;
    }

    private void OnDisable()
    {
        hm.HandleLivesChange -= BeginParticles;
    }

    private void BeginParticles(int lives)
    {
        // only when the puddle-object is active and the empty GO "ParticleChecker" is active as well, the particles are instantiated
        if (this.transform.GetChild(0).gameObject.activeSelf && this.transform.GetChild(2).gameObject.activeSelf)
        {
            Instantiate(bloodParticles, transform.position, transform.rotation, particleParent.transform);
            //GO ParticleChecker is set to inactive, so it won't cause the particle effect a second time
            this.transform.GetChild(2).gameObject.SetActive(false);

        }
    }
}
