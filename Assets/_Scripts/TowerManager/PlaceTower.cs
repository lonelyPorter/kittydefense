﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Places a tower if on valid ground.
/// Shows Errormessage if not
/// </summary>
public class PlaceTower : MonoBehaviour
{
    Rigidbody playerRigitbody;
    ScoreManager scoreManager;
    GameObject parent;

    //Parameter for InfoMessage
    bool displayMessage = false;
    float displayTime = 3f;
    string message = "";

    //Particle
    [SerializeField]
    GameObject towerGrowParticles;
    private GameObject particleParent;

    // Audio related fields
    private AudioManager audioManager;
    [SerializeField]
    private string placeTowerSound = "PlaceTower";
    private string errorSound = "Error";

    public event Action<String> HandleShowMessage = delegate { };
    public static event Action HandleTowerBuilt = delegate { };

    private void Awake()
    {
        particleParent = GameObject.Find("Particles");
        parent = new GameObject("Towers");

        if (particleParent == null)
        {
            Debug.LogError("GameObject Particles not found");
        }
    }

    void Start()
    {
        audioManager = AudioManager.instance;
        playerRigitbody = GetComponent<Rigidbody>();
        scoreManager = FindObjectOfType<ScoreManager>();
        if (scoreManager == null)
        {
            Debug.LogWarning("No ScoreManager found in Scene!");
        }
    }

    private void OnDestroy()
    {
        HandleTowerBuilt = delegate { };
    }

    private void Update()
    {
        displayTime -= Time.deltaTime;
        if (displayTime <= 0)
        {
            displayMessage = false;
        }
    }

    /// <summary>
    /// Places the given tower
    /// </summary>
    /// <param name="towerToPlace">Tower Prefab</param>
    /// <param name="price">Cost of the tower</param>
    public void Place(GameObject towerToPlace, int price)
    {
        if (towerToPlace != null)
        {
            if (!IsOnEnemyPath())
            {
                if (!IsObstacleInFrontOfThePlayer())
                {
                    if (scoreManager.DecreaseScore(price))
                    {
                        HandleTowerBuilt?.Invoke();
                        Animator animator = towerToPlace.GetComponent<Animator>();
                        if (animator != null)
                            animator.SetBool("IsGrowing", true);
                        Instantiate(towerToPlace, GetTowerPosition(), Quaternion.identity, parent.transform);
                        audioManager.PlaySound(placeTowerSound);
                        if (particleParent == null)
                        {
                            particleParent = new GameObject("ParticleParent");
                        }

                        Destroy(Instantiate(towerGrowParticles, GetTowerPosition(), Quaternion.LookRotation(transform.up), particleParent.transform), 2f);
                    }
                    else
                    {
                        HandleShowMessage?.Invoke("I can't afford that");
                    }
                }
                else
                {
                    HandleShowMessage?.Invoke("There is something in the way");
                }
            }
            else
            {
                HandleShowMessage?.Invoke("Sorry, I don't know how to build on paths");
            }
        }
    }

    private Vector3 GetPlayerPosition()
    {
        return playerRigitbody.transform.position;
    }

    private Vector3 GetTowerPosition()
    {
        return new Vector3(GetPlayerPosition().x - 3, GetPlayerPosition().y, GetPlayerPosition().z - 3);
    }

    private bool IsObstacleInFrontOfThePlayer()
    {
        Collider[] colliderAtnewTowerPosition = Physics.OverlapSphere(new Vector3(GetTowerPosition().x, GetTowerPosition().y + 0.5f, GetTowerPosition().z), 0.25f);
        return colliderAtnewTowerPosition.Length != 0;
    }

    private bool IsOnEnemyPath()
    {
        Physics.Raycast(new Vector3(GetTowerPosition().x, GetTowerPosition().y + 1f, GetTowerPosition().z), Vector3.down, out RaycastHit hit);
        return hit.collider == null ? false : hit.collider.gameObject.CompareTag("noTowersAllowed");
    }
}
