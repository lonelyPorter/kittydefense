﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fills the TowerMenu UI with Towers, sets up onClick button listeners
/// </summary>
public class TowerMenuManager : MonoBehaviour
{
    private Canvas canvas;
    private TowerManager tm;

    [SerializeField]
    private List<SingleTowerUI> towerUIElements;

    private void Awake()
    {
        //Loading canvas and disabling it (Has to be enabled in Prefab to be found)
        canvas = GetComponentInChildren<Canvas>();
        canvas.gameObject.SetActive(false);

        tm = GetComponent<TowerManager>();
    }

    public void Initialize(List<SOTower> towers)
    {
        int i = 0;
        foreach (SingleTowerUI towerUI in towerUIElements)
        {
            SOTower tower = towers[i];
            int towerIndex = i;
            if (tower != null)
            {
                towerUI.UpdateTitle(tower.Title);
                towerUI.UpdateDescription(tower.Description);
                towerUI.UpdatePrice(tower.Price);
                towerUI.UpdateImage(tower.ImageButton);
                towerUI.AddListener(delegate { TowerButtonPressed(towerIndex); });
            }
            else
            {
                towerUI.gameObject.SetActive(false);
            }
            i++;
        }
    }

    private void Update()
    {
        //Toggeling canvas State when T pressed
        if (Input.GetKeyDown(KeyCode.T))
        {
            canvas.gameObject.SetActive(!canvas.gameObject.activeSelf);
        }

    }

    private void TowerButtonPressed(int index)
    {
        tm.buildTower(index);
    }
}
