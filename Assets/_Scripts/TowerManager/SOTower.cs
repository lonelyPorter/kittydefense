﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Tower", menuName = "Tower")]
public class SOTower : ScriptableObject
{
    [SerializeField]
    private Sprite imageButton;
    [SerializeField]
    private string title;
    [SerializeField]
    private string description;
    [SerializeField]
    private int price;

    [SerializeField]
    GameObject tower;
    [SerializeField]
    GameObject towerGhost;

    public Sprite ImageButton { get => imageButton; }
    public string Title { get => title; }
    public string Description { get => description; }
    public int Price { get => price; }

    public GameObject Tower { get => tower; }
    public GameObject TowerGhost { get => towerGhost; }
}
