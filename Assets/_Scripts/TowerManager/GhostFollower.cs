﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Makes a Ghost always follow the Player
/// </summary>
public class GhostFollower : MonoBehaviour
{
    private GameObject player;
    private Vector3 offset = new Vector3(-3, 0, -3);

    private void Awake()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}
