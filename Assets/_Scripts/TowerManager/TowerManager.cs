﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Holds all the Towers the Player is allowed to build
/// </summary>
public class TowerManager : MonoBehaviour
{
    [SerializeField]
    private List<SOTower> towers;

    [SerializeField]
    private PlaceTower player;

    private TowerMenuManager towerMenuManager;
    private GameObject ghostParent;
    private int activeGhost = -1;
    private List<GameObject> towerGhosts;

    private void Start()
    {
        towerMenuManager = GetComponent<TowerMenuManager>();
        towerMenuManager.Initialize(towers);
        InitalizeGhosts();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            ShowGhost(0);
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            ShowGhost(1);
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            ShowGhost(2);
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            ShowGhost(3);
        else if (Input.GetKeyDown(KeyCode.Alpha5))
            ShowGhost(4);
        else if (Input.GetKeyDown(KeyCode.Alpha6))
            ShowGhost(5);
        else if (Input.GetKeyDown(KeyCode.Return))
            buildTower(activeGhost);
    }

    private void InitalizeGhosts()
    {
        ghostParent = new GameObject("Ghosts");
        towerGhosts = new List<GameObject>();

        foreach (SOTower tower in towers)
        {
            if (tower != null)
            {
                GameObject ghost = Instantiate(tower.TowerGhost, ghostParent.transform);
                ghost.SetActive(false);
                towerGhosts.Add(ghost);
            }
        }
    }

    private void ShowGhost(int index)
    {
        if (towerGhosts.Count <= index)
            return;
        if (activeGhost == -1)
        {
            //When no ghost selected, show ghost
            towerGhosts[index].SetActive(true);
            activeGhost = index;
        }
        else if (activeGhost == index)
        {
            //If Active Ghost selected again, "unshow" it
            towerGhosts[index].SetActive(false);
            activeGhost = -1;
        }
        else
        {
            //If another Ghost selected, switch
            towerGhosts[activeGhost].SetActive(false);
            towerGhosts[index].SetActive(true);
            activeGhost = index;
        }
    }

    public void buildTower(int index)
    {
        if (index >= 0)
        {
            SOTower tower = towers[index];
            player.Place(tower.Tower, tower.Price);
        }
    }
}
