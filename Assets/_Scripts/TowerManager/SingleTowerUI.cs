﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SingleTowerUI : MonoBehaviour
{
    [SerializeField]
    private Button imageButton;

    [SerializeField]
    private TextMeshProUGUI title;
    [SerializeField]
    private TextMeshProUGUI description;
    [SerializeField]
    private TextMeshProUGUI price;

    public void UpdateImage(Sprite newImage)
    {
        Image image = imageButton.gameObject.GetComponent<Image>();
        image.sprite = newImage;
    }

    public void UpdateTitle(string newTitle)
    {
        title.SetText(newTitle);
    }

    public void UpdateDescription(string newDescription)
    {
        description.SetText(newDescription);
    }

    public void UpdatePrice(int newPrice)
    {
        price.SetText(newPrice + "Coins");
    }

    public void AddListener(UnityEngine.Events.UnityAction listener)
    {
        imageButton.onClick.AddListener(listener);
    }
}
